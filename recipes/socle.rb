#
# Cookbook Name:: es-cookbook
# Recipe:: socle
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

template '/etc/apt/sources.list.d/socle.list' do 
  source 'socle.erb'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

include_recipe 'apt'
package 'Install Java' do
 package_name node['es-cookbook']['socle']['java']['pkg']['name']
# version node['es-cookbook']['socle']['java']['version']
 options('--force-yes')
 action :upgrade
end
bash 'update alternative' do
 user 'root'   
 code <<-EOH
 sudo update-alternatives --set java #{node['es-cookbook']['socle']['java']['alternative']}
  java -version 2>&1 | grep #{node['es-cookbook']['socle']['java']['version']}
  EOH
  only_if do     
  node['es-cookbook']['socle']['java'].key?('alternative') &&
  !node['es-cookbook']['socle']['java']['alternative'].nil? &&       
  !node['es-cookbook']['socle']['java']['alternative'].empty?
 end
end
bash 'get_java_certificate' do
 code <<-EOH
   openssl x509 -in <(openssl s_client -connect chef001.admin.prod.gen01.ke.p.fti.net:443 -prexit 2>/dev/null) -out /tmp/chef001.prod.crt
    keytool -importcert -file /tmp/chef001.prod.crt -alias chef001prod -keystore $(readlink -f /usr/bin/java | sed "s:/bin/java::")/lib/security/cacerts -storepass changeit -noprompt
 EOH
  not_if 'keytool -list -keystore ' \
     '$(readlink -f /usr/bin/java | sed "s:/bin/java::")' \
      '/lib/security//cacerts -noprompt -alias chef001prod ' \
       ' -storepass changeit'
end    
  
