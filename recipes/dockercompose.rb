

#Install docker-compse



include_recipe 'docker_compose::installation'

cookbook_file '/ect/elastic.yml' do
 source 'elastic.yml'
 owner 'root'
 group 'root'
 mode '0755'
 action :create
 notifies :up, 'docker_compose_application[ES]', :delayed
end

docker_compose_application 'ES' do 
 action :up
 compose_files ['/ect/elastic.yml' ] 
end


