%w(
   node['elasticsearch']['configure']['path_home']}
    node['elasticsearch']['configure']['path_conf']
    node['elasticsearch']['configure']['path_logs']
    node['elasticsearch']['configure']['path_pid']
    node['elasticsearch']['configure']['path_plugins']
    node['elasticsearch']['configure']['path_bin']
    node['elasticsearch']['configure']['path_data']
).each do |dir|
  directory dir do
    owner 'root'
    group 'root'
    recursive true
    mode '0777'
    action :create
  end
end
directory "/var/opt/elasticsearch" do
 recursive true
  mode '0777'
 action :create
end

elasticsearch_user node['elasticsearch']['user']['username'] do
  username node['elasticsearch']['user']['username']
  groupname node['elasticsearch']['user']['groupname']
  shell '/bin/bash'
  action :create
end

elasticsearch_install 'elasticsearch' do
  #dir   package: node['elasticsearch']['configure']['path_home'] 
  download_url node['elasticsearch']['install']['download_url']
  download_checksum node['elasticsearch']['install']['download_checksum']
  version '2.4.4'
  action :install
end

elasticsearch_configure 'elasticsearch' do
 allocated_memory '256m'
 configuration ({
    'node.name' =>  "DICKOS",
    'cluster.name' => 'ES-Madicke',
    'http.port' => 9200,
    'path.repo' =>  ["/var/opt/elasticsearch"]
  })
 java_home node['es-cookbook']['socle']['java']['home']
  action :manage
end

elasticsearch_service 'elasticsearch' do
  service_actions [:enable, :start]
end

elasticsearch_plugin 'cerebro' do
 url ' https://github.com/lmenezes/cerebro/releases/cerebro-0.6.5.zip'
 action :install
end

