# Cookbook Name:: es-cookbook
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
#Chef::Recipe.send(:include, ::Myclient)

#Chef::Resource::RubyBlock.send(:include, ::ESCalcul)

Chef::Recipe.send(:include, ::ESCalcul)
Chef::Resource.send(:include, ::ESCalcul)
Chef::Provider.send(:include, ::ESCalcul)
Opscode.send(:include, ::ESCalcul)

if node['es-cookbook']['database']['mysql']['enable'] 
  include_recipe 'es-cookbook::database'
else
#Chef::Recipe.send(:include, ::Indexation)
#Chef::Resource.send(:include, ::Indexation)
#Chef::Provider.send(:include, ::Indexation)
#Opscode.send(:include, ::Indexation)

if node['es-cookbook']['docker']['enable'] == false
  if node['es-cookbook']['docker']['mode_compose'] 
     include_recipe 'es-cookbook::dockercompose'
  elsif
     include_recipe 'es-cookbook::docker'
     get_image(node['es-cookbook']['docker']['es_image'],node['es-cookbook']['docker']['es_image_tag'])
     es_params = {
        image: node['es-cookbook']['docker']['es_image'],
        port: node['es-cookbook']['docker']['es_port']
     }
    des_contenair(es_params)
  end
elsif
  include_recipe 'es-cookbook::socle'
  include_recipe 'es-cookbook::elasticsearch'
end


cookbook_file '/opt/content.txt' do
  source 'content.txt'
  mode '0755'
  action :create
end
chef_gem 'elasticsearch' 
chef_gem 'apachelogregex' 

ruby_block 'Create index' do
   block do
     indices = "#{node['es-cookbook']['data_bag']}_client"
     clients = data_bag(indices)
     clients.each do | client |
       indice = data_bag_item(indices,client)
       values = indice['settings'].values
       indice_name = indice['id']
       Indexation.create_indice(indice_name,values[0],values[1])
       Indexation.get_settings(indice_name)
       Indexation.put_mapping(indice_name,indice['type'],indice['mapping'])
        puts "#############################################################################"
       Indexation.get_mapping(indice_name,indice['type'])
       Indexation.parsing("/opt/content.txt",indice_name,indice['type'])
       puts "#############################################################################"
       puts  get_nb_timeout(indice_name)
       puts get_nb_requetes_reussies(indice_name)
       puts get_requesttime_byclient(indice_name,'content')
       puts get_networkBp_byclient(indice_name,'content')
     end
   end
end
end
##include_recipe 'es-cookbook::kibana'
