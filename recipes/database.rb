include_recipe 'apt'

mysql_client 'default' do
 action :create
end

mysql_service 'default' do
  initial_root_password node['es-cookbook']['database']['userpassword'] 
 action [:create,:start]
end

mysql2_chef_gem 'default' do
 action :install
end

passwords = data_bag_item('passwords','mysql')

mysql_connection_info = {
 :host => node['es-cookbook']['database']['host'],
 :username => node['es-cookbook']['database']['username'],
 :password => passwords['root_password']
}

mysql_database node['es-cookbook']['database']['db_name'] do
 connection mysql_connection_info
 action :create
end

mysql_database_user node['es-cookbook']['database']['db_name'] do
 connection mysql_connection_info
 password passwords['admin_password']
 database_name node['es-cookbook']['database']['db_name']
 host node['es-cookbook']['database']['host']
 action [:create,:grant]


end

cookbook_file '/tmp/create-tables.sql' do
 source 'create-tables.sql'
end

execute "Insert data in database #{node['es-cookbook']['database']['db_name']}" do
 command "mysql -h 127.0.0.1 -u #{node['es-cookbook']['database']['username']} -p#{passwords['root_password']} -D #{node['es-cookbook']['database']['db_name']} < /tmp/create-tables.sql"
not_if "mysql -h 127.0.0.1 -u #{node['es-cookbook']['database']['username']} -p#{passwords['root_password']} -D #{node['es-cookbook']['database']['db_name']} -e 'describe customers;'"
end
