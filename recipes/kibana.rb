

filename = node['kibana']['install']['download_url'].split('/').last

remote_file "#{Chef::Config[:file_cache_path]}/#{filename}"do 
 source node['kibana']['install']['download_url']
 mode '0644'
 checksum node['kibana']['install']['download_checksum']
end

dpkg_package "#{Chef::Config[:file_cache_path]}/#{filename}" do
 action :install
 options '--force-confdef'
end

service 'kibana' do
 supports status: true, restart: true
 action [:enable, :start]
end

template node['kibana']['conf'] do 
 source 'kibana.erb'
 owner 'root'
 group 'root'
 mode '0644'
end

