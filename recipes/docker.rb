# Install docker
template '/etc/apt/sources.list.d/docker.list' do
 source 'docker.erb'
 owner 'root'
 group 'root'
 mode '0755'
end

bash 'Update cache' do
 user 'root' 
 code <<-EOH 
  apt-get update
 EOH
end

docker_installation_package 'default' do
 package_name node['es-cookbook']['docker']['name']
 package_version node['es-cookbook']['docker']['version']
 package_options node['es-cookbook']['docker']['options']
 action :create
end

docker_service_manager 'default' do
 action :start
end


