def random_password
  require 'securerandom'
  SecureRandom.base64
end

default['es-cookbook']['database']['mysql']['enable'] = true
default['es-cookbook']['database']['db_name'] = 'mysql_db'
default['es-cookbook']['database']['username'] = 'root'
default['es-cookbook']['database']['userpassword'] = "fakerootpassword"
default['es-cookbook']['database']['admin_password'] = random_password
default['es-cookbook']['database']['host'] = '127.0.0.1'
