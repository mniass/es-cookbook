require 'elasticsearch'
require 'apachelogregex'

class Chef::Recipe::Indexation
         @@client = Elasticsearch::Client.new
	 def self.get_cluster_health
	    return @@client.cluster.health['status']
	 end
         def self.recherche_alias(name)
            aliases = @@client.indices.get_aliases
            aliases.keys.each do | i |
              if (i.split('_'))[0] == name
                  return true
              end
            end
         return false
         end
         def self.put_settings(name,nb_replicas,nb_shards)
            if recherche_alias(name) == true
               indice = get_indice(name)
               @@client.indices.put_settings index: indice, body: {index:  { number_of_replicas: nb_replicas, number_of_shards: nb_shards }}        
            end  
         end
         def self.get_settings(name)
           indice = get_indice(name)
           puts @@client.indices.get_settings index: indice
         end 
	 def self.create_indice(name,nb_replicas,nb_shards)
	   if get_cluster_health !='red'
              if recherche_alias(name) == false
                 @@client.indices.create index: name + '_v1', body: { settings: { index:  { number_of_replicas: nb_replicas, number_of_shards: nb_shards }}}
                 @@client.indices.update_aliases body: {
                    actions: [
                     { add: { index: name + '_v1' , alias: name } }
                   ]    
                  }  
              elsif
                version = (get_indice(name)).split('_v').last
                ancienne_version = (version).to_i
                nouvelle_version = ancienne_version+1
                if nouvelle_version > 10
                  nouvelle_version = 1 
                end
                @@client.indices.create index: name + '_v' + nouvelle_version.to_s , body: { settings: { index:  { number_of_replicas: nb_replicas, number_of_shards: nb_shards }}}
                  @@client.indices.update_aliases body: {
                     actions: [
                     { remove: { index: name + '_v' + ancienne_version.to_s, alias: name } },
                     { add: { index: name + '_v' + nouvelle_version.to_s , alias: name } }
                  ]    
                  }
              @@client.indices.delete index: name + '_v' + ancienne_version.to_s     
              end
	   end
	 end

	 def self.get_indice(myalias)
            return  ((@@client.indices.get_alias name: myalias).keys)[0]
         end
	 def self.put_mapping(indice,mytype,definition)
          name = get_indice(indice)
	  if @@client.indices.exists? index: name
	    @@client.indices.put_mapping index: name , type: mytype , body: definition 
	 end
        end
	def self.get_mapping(indice, mytype)
          name = get_indice(indice)
	  puts @@client.indices.get_mapping index: name, type: mytype  
	end

	def self.put_doc(indice,mytype,id,clientip,indicename,timestamp,verb,request,httpversion,response,bytes,requesttime)
          name = get_indice(indice)
	  @@client.bulk body: [ 
	       { index: { _index: name, _type: mytype, _id: id }},{clientip: clientip,indicename: indicename ,timestamp: timestamp ,verb: verb,request: request, httpversion: httpversion, response: response, bytes: bytes, requesttime: requesttime }
	 ]
	end

	def self.parsing(file,indice,mytype)
	 i=1
	 format = '%h %u %t \"%r\" %s %b %T'
	 parser = ApacheLogRegex.new(format)
	 File.readlines(file).collect do |line|
	   result =  parser.parse(line)
	   verb = result['%r'].split(" ")[0]
	   request = result['%r'].split(" ")[1]
	   httpversion = result['%r'].split(" ")[2]
	   put_doc(indice,mytype,i,result['%h'],result['%u'],result['%t'],verb,request,httpversion,result['%s'],result['%b'],result['%T'])
           i=i+1
	 end
	end
       
	def self.create_snapshot_repo(name,location)
	 @@client.snapshot.create_repository repository: name,
		  body: {
		      type: 'fs',
			settings: { location: location, compress: true }
		  }
	end
	def self.get_snapshot_repo(repo)
	  puts @@client.snapshot.get_repository repository: repo
	end
	def self.create_snapshot(indice,repo)
         name = get_indice(indice)
	 @@client.snapshot.create repository: repo, snapshot: name, body: { indices: indice, ignore_unavailable: true }
	end
	def self.get_snapshot(repo)
	 puts @@client.snapshot.get repository: repo, snapshot: '_all'
	end

end
