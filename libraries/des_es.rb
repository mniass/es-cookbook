require 'elasticsearch'

class Chef::Recipe::MyClient
 @@client = Elasticsearch::Client.new
 
 def self.get_cluster_health
    return @@client.cluster.health['status']
 end
 def self.create_indice(name,nb_replicas,nb_shards) 
   if get_cluster_health !='red' && !(@@client.indices.exists? index: name)
     @@client.indices.create index: name , body: { number_of_replicas: nb_replicas, number_of_shards: nb_shards }
   end
 end
 def self.add_mapping(indice,mytype)
   if @@client.indices.exists? index: indice 
      if (@@client.indices.get_mapping index: indice)[indice]['mappings'][mytype].nil?
        if indice == 'content'
         @@client.indices.put_mapping index: indice , type: mytype , body: {
         properties: {
           title: { type: 'string' }, 
           name: { type: 'string' },
           age: { type: 'integer' } 
         }
       } 
       elsif indice == 'game'
          @@client.indices.put_mapping index: indice , type: mytype , body: {
              properties: {
                duree: { type: 'string' }, 
                name: { type: 'string' },
                acteur: { type: 'string' } 
              }
            }
      end 
   end
 end
end
 def self.get_mapping(indice)
   puts   @@client.indices.get_mapping index: indice
 end  
 def self.get_cluster_status
    puts @@client.indices.status
 end
 def self.get_shard_infos(indice)
    puts @@client.cat.shards index: indice
 end
 def self.get_nb_docs(indice)
    puts @@client.cat.count index: indice
 end
 def self.put_docs(indice,mytype,id,*tab)
   if indice == 'content'
    @@client.bulk body: [
      { index: { _index: indice, _type: mytype, _id: id }}, { title: tab[0], name: tab[1], age: tab[2] } 
    ]
   elsif indice == 'game'
     @@client.bulk body: [
      { index: { _index: indice, _type: mytype, _id: id }}, { duree: tab[0] , name: tab[1], acteur: tab[2] }
    ]
   end
 end 
 def self.get_docs(indice,id,mytype)
  puts @@client.search index: indice, type: mytype, body: {
    query: { match: { _id: id } }
  } 
 end 
 def self.delete_indice(indice)
    @@client.indices.delete index: indice
 end
def self.create_snapshot_repo(name, repo)
  @@client.snapshot.create_repository repository: name, body: {
     type: 'fs',
     settings: { location: repo, compress: true }
   }
end 
def self.create_snapshot(indice,repo)
   @@client.snapshot.create repository: repo,
          snapshot: indice , body: { indices: indice, ignore_unavailable: true }, wait_for_completion: true
end
def self.get_snapshot(repo)
  puts @@client.snapshot.get repository: repo, snapshot: '_all'
end

def self.get_nodes_info
 puts (((@@client.nodes.info http: true)['nodes'])['7dVY9lIITl6tPgArxm-MFA'])['http']['max_content_length_in_bytes']
end
def self.put_alias(indice,monalias)
 @@client.indices.update_aliases body: {
   actions: [{ add: { index: indice, alias: monalias }}] 
 }
end
def self.get_alias(indice)
 @@client.indices.get_aliases index: indice
end
def self.remove_alias(indice,monalias)
 @@client.indices.update_aliases body: {
    actions: [
       { remove: { index: indice, alias: monalias } } 
  ]
 }
end
end
