
class Chef::Recipe
  def get_image(limage,ltag)
     docker_image limage do
     tag ltag
     action :pull_if_missing
  end
  
  def des_contenair(es_params)
   docker_container 'elasticsearch' do 
    container_name node['es-cookbook']['docker']['es_container']
    port es_params[:port]
    image es_params[:image] + ":#{node['es-cookbook']['docker']['es_image_tag']}"
    action :run
   end
  end
end
end
