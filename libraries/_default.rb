begin  
   gem 'elasticsearch', '= 2.0.0'
rescue LoadError
  run_context = Chef::RunContext.new(
   Chef::Node.new, {},    
   Chef::EventDispatch::Dispatcher.new)
require 'chef/resource/chef_gem'
  chefgem = Chef::Resource::ChefGem.new('elasticsearch', run_context) 
 chefgem.version '2.0.0'
  chefgem.compile_time true if chefgem.respond_to?(:compile_time)  
 chefgem.options '--clear-sources --source http://artifactory.packages.install-os.multis.p.fti.net/sfy-idem_ruby_estack' 
 chefgem.run_action(:install)
end


begin
   gem 'apachelogregex', '= 0.1.0'
rescue LoadError
  run_context = Chef::RunContext.new(
   Chef::Node.new, {},
   Chef::EventDispatch::Dispatcher.new)
require 'chef/resource/chef_gem'
  chefgem = Chef::Resource::ChefGem.new('apachelogregex', run_context)
 chefgem.version '0.1.0'
  chefgem.compile_time true if chefgem.respond_to?(:compile_time)
 chefgem.options '--clear-sources --source http://rubygems.org'
 chefgem.run_action(:install)
end
