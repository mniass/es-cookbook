require 'elasticsearch'
    module ESCalcul
       @@client = Elasticsearch::Client.new
       def get_indice(myalias)
            return  ((@@client.indices.get_alias name: myalias).keys)[0]
       end
       def get_nb_timeout(ialias)
          indice = get_indice(ialias)
          i=1
          nb_time_out =0
          nb_docs = (@@client.count index: indice)['count']
         while( i <= nb_docs)
             rt = (((@@client.get index: indice, id: i.to_s, fields: 'requesttime')['fields'])['requesttime'])[0]
             if rt >= 2000
               nb_time_out = nb_time_out+1 
             end
             i=i+1
         end
         return nb_time_out
       end
       def get_nb_requetes_reussies(ialias)
          indice = get_indice(ialias)
          i=1
          nb_requetes_reussies =0
          nb_docs = (@@client.count index: indice)['count']
         while( i <= nb_docs)
             response = (((@@client.get index: indice, id: i.to_s, fields: 'response')['fields'])['response'])[0]
             if response == 200
               nb_requetes_reussies+=1
             end
             i=i+1
         end
         return nb_requetes_reussies
       end
       def get_requesttime_byclient(ialias,client)
         indice = get_indice(ialias)
        i=1 
        requesttime_byclient=0
        nb_docs = (@@client.count index: indice)['count']
        while( i <= nb_docs)
           doc = @@client.get index: indice, id: i.to_s
           if ((doc["_source"])["indicename"] == client)
                requesttime_byclient = requesttime_byclient + ((doc["_source"])["requesttime"]).to_i
           end
           i+=1
        end
        return requesttime_byclient
       end
       def get_networkBp_byclient(ialias,client)
         indice = get_indice(ialias)
        i=1
        networkBp_byclient=0
        nb_docs = (@@client.count index: indice)['count']
        while( i <= nb_docs)
           doc = @@client.get index: indice, id: i.to_s
           if ((doc["_source"])["indicename"] == client)
                networkBp_byclient = networkBp_byclient + ((doc["_source"])["bytes"]).to_i
           end
           i+=1
        end
        return networkBp_byclient
       end
       
   end
